# OSI model
* __OSI__ stands for __Open System Interconnection__ model.
* It is used to understand how the data is transferred from one computer to another in a computer network.
* It is seven layer model introduced by __International Organization for Standardization (ISO)__ <br> in 1984.
* It consists of following seven layers and these layers are further categorized.
  * Software Layers
    * Application Layer
    * Presentation Layer
    * Session Layer
  * Heart of OSI
    * Transport Layer
  * Hardware Layers
    * Network Layer
    * Data Layer
    * Physical Layer
* Each layer is a packet of protocols.

### 1 - Application Layer
At the very top of the OSI Reference Model stack of layers, we find Application layer which is implemented by the network applications. These applications produce the data, which has to be transferred over the network. This layer also serves as a window for the application services to access the network and for displaying the received information to the user. This layer is also known as __Desktop Layer__.
Ex: Application – Browsers, Skype Messenger etc.



### 2 - Presentation Layer
Presentation layer is also called the __Translation layer__.The data from the application layer is extracted here and manipulated as per the required format to transmit over the network.
* The functions of the presentation layer are :
  * __Translation__ : For example, ASCII to EBCDIC.
  * __Encryption/Decryption__ : Data encryption translates the data into another form or code. The encrypted data is known as the cipher text and the decrypted data is known as plain text. A key value is used for encrypting as well as decrypting data.
  * __Compression__ : Reduces the number of bits that need to be transmitted on the network.


### 3 -Session Layer
This layer is responsible for establishment of connection, maintenance of sessions, authentication and also ensures security.
* The functions of the session layer are :

  * __Session establishment, maintenance and termination__ : The layer allows the two processes to establish, use and terminate a connection.

  * __Synchronization__ : This layer allows a process to add checkpoints which are considered as synchronization points into the data. These synchronization point help to identify the error so that the data is re-synchronized properly, and ends of the messages are not cut prematurely and data loss is avoided.

  * __Dialog Controller__ : The session layer allows two systems to start communication with each other in half-duplex or full-duplex.


### 4 - Transport Layer
Transport layer provides services to application layer and takes services from network layer. The data in the transport layer is referred to as Segments. It is responsible for the End to End Delivery of the complete message. The transport layer also provides the acknowledgement of the successful data transmission and re-transmits the data if an error is found.
* The functions of the transport layer are :

  * __Segmentation and Reassembly__ : This layer accepts the message from the (session) layer , breaks the message into smaller units . Each of the segment produced has a header associated with it. The transport layer at the destination station reassembles the message.
  * __Service Point Addressing__ : In order to deliver the message to correct process, transport layer header includes a type of address called service point address or port address. Thus by specifying this address, transport layer makes sure that the message is delivered to the correct process.

* The services provided by the transport layer :

  * __Connection Oriented Service__ : In this type of transmission, the receiving device sends an acknowledgement, back to the source after a packet or group of packet is received. This type of transmission is reliable and secure.
  It is a three-phase process which include :
    * Connection Establishment
    * Data Transfer
    * Termination / disconnection
    
  * __Connection less service__ : It is a one-phase process and includes Data Transfer. In this type of transmission, the receiver does not acknowledge receipt of a packet. This approach allows for much faster communication between devices. Connection-oriented service is more reliable than connectionless Service.

### 5 - Network Layer
Network layer works for the transmission of data from one host to the other located in different networks. It also takes care of packet routing i.e. selection of the shortest path to transmit the packet, from the number of routes available. The sender & receiver’s IP address are placed in the header by the network layer.
* The functions of the Network layer are :
  * __Routing__ : The network layer protocols determine which route is suitable from source to destination. This function of network layer is known as routing.
  * __Logical Addressing__ : In order to identify each device on internetwork uniquely, network layer defines an addressing scheme. The sender & receiver’s IP address are placed in the header by network layer. Such an address distinguishes each device uniquely and universally.


### 6 - Data Layer
The data link layer is responsible for the node to node delivery of the message. The main function of this layer is to make sure data transfer is error-free from one node to another, over the physical layer. When a packet arrives in a network, it is the responsibility of DLL to transmit it to the Host using its MAC address.
* Data Link Layer is divided into two sub layers :

  1. Logical Link Control (LLC)
  2. Media Access Control (MAC)

* The functions of the data Link layer are :

  * __Framing__ : Framing is a function of the data link layer. It provides a way for a sender to transmit a set of bits that are meaningful to the receiver. This can be accomplished by attaching special bit patterns to the beginning and end of the frame.
  * __Physical addressing__ : After creating frames, Data link layer adds physical addresses (MAC address) of sender and/or receiver in the header of each frame.
  * __Error control__ : Data link layer provides the mechanism of error control in which it detects and retransmits damaged or lost frames.
  * __Flow Control__ : The data rate must be constant on both sides else the data may get corrupted thus , flow control coordinates that amount of data that can be sent before receiving acknowledgement.
  * __Access control__ : When a single communication channel is shared by multiple devices, MAC sub-layer of data link layer helps to determine which device has control over the channel at a given time.


### 7 - Physical Layer 
The lowest layer of the OSI reference model is the physical layer. It is responsible for the actual physical connection between the devices. The physical layer contains information in the form of __bits__. It is responsible for transmitting individual bits from one node to the next. When receiving data, this layer will get the signal received and convert it into 0s and 1s and send them to the Data Link layer, which will put the frame back together.
* The functions of the physical layer are :

  * __Bit synchronization__ : The physical layer provides the synchronization of the bits by providing a clock. This clock controls both sender and receiver thus providing synchronization at bit level.
  * __Bit rate control__ : The Physical layer also defines the transmission rate i.e. the number of bits sent per second.
  * __Physical topologies__ : Physical layer specifies the way in which the different, devices/nodes are arranged in a network i.e. bus, star or mesh topolgy.
  * __Transmission mode__ : Physical layer also defines the way in which the data flows between the two connected devices. The various transmission modes possible are: Simplex, half-duplex and full-duplex.


References to Above documentation [Video](https://www.youtube.com/watch?v=vv4y_uOneC0) & [Docs](https://www.geeksforgeeks.org/layers-of-osi-model/)
